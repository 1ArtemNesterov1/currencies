package com.example.hakaton.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hakaton.R;
import com.example.hakaton.model.RatesModel;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CurrrencyAdapter extends RecyclerView.Adapter<CurrrencyAdapter.ViewHolder> {
    List<RatesModel> modelCurrencies;
    Context context;
    Boolean showRates;
    RatesModel ratesModel;
    private OnItemClickListener listener;
    private LongClickListener longItemClickListener;


    //кунструктор для получения данных список моделек.контекс.булиан-если фолс не показывает строчку.слушатель.длинное нажатие
    public CurrrencyAdapter(List<RatesModel> modelCurrencies, Context context, Boolean showRates,
                            OnItemClickListener listener, LongClickListener longItemClickListener) {

        this.modelCurrencies = modelCurrencies;
        this.context = context;
        this.showRates = showRates;

        this.listener = listener;
        this.longItemClickListener = longItemClickListener;
    }

    //метод для округления
    private static String convertNumberToString(Number value) {
        Locale locale = Locale.ENGLISH;
        NumberFormat numberFormat = NumberFormat.getInstance(locale);
        numberFormat.setMinimumFractionDigits(2); // trailing zeros
        numberFormat.setMaximumFractionDigits(3); // round to 2 digits

        return numberFormat.format(value);
    }

    //метод когда создаеться вьюхолдер
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        //метод для привязки лаяута к элементу списка или диалогу
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_item, viewGroup, false);

    //каждый элемент будет новым
        return new ViewHolder(view);
    }


    //прывязка к йдишнику в лайауте к вайнд вью бай айди(переменная класса)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder view, int position) {
        view.bind(position);
    }


    @Override
    public int getItemCount() {
        return modelCurrencies.size();
    }

    public interface LongClickListener {
        void longItemClick(RatesModel ratesModel);
    }

    //интерфейс для слушателя
    public interface OnItemClickListener {
        void OnItemClick(RatesModel ratesModel);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rv_currency_image)
        ImageView imageView;
        @BindView(R.id.rv_currency_name)
        TextView currencyName;
        @BindView(R.id.rv_currency_description)
        TextView currencyDescription;
        @BindView(R.id.rv_rateCurrency)
        TextView rateCurrency;


        //привязка каждого элемента
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        //заполнение данными
        public void bind(int position) {


            //список с сервера где будет приходить модельки
            final RatesModel ratesModel = modelCurrencies.get(position);

            //установление имя(текс)
            currencyName.setText(ratesModel.getCurrencyTitle());

            //установление картинки
            imageView.setImageResource(ratesModel.getCurrencyIcon());


            //

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    if (longItemClickListener != null) {
                        longItemClickListener.longItemClick(ratesModel);
                        return true;
                    }
                    return false;
                }
            });

            //реагирование на нажатие вьюхи(джава слушатель)
            itemView.setOnClickListener(new View.OnClickListener() {

                //для собственого слушателя передаем данные
                @Override
                public void onClick(View v) {
                    listener.OnItemClick(ratesModel);
                }
            });
            //если в шоу райтес = тру.то
            if (showRates) {
                //текс райткаренси  делаем видимым
                rateCurrency.setVisibility(View.VISIBLE);
                String rate = convertNumberToString(ratesModel.getCurrencyRate());
                //конвектируем наш булиан в нормальныый вид 27.2 округление до 3 символов
                rateCurrency.setText(rate);

                //если фолс то текс райткаренси делаем не видимым
            } else rateCurrency.setVisibility(View.GONE);
        }
    }

}
