package com.example.hakaton.Utils;

import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import com.example.hakaton.MainActivity;
import com.example.hakaton.R;
import com.example.hakaton.adapter.CurrrencyAdapter;

public class Utils {

    private boolean onClickRetry = false;

    public boolean isOnClickRetry() {
        return onClickRetry;
    }

    public void ErrorInternet(final CurrrencyAdapter adapterCurrencys, Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Error")
                .setMessage("Error with internet. Check your connection and press <<Retry>>")
                .setCancelable(false)
                //слушатель на кнопку
                .setPositiveButton("retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onClickRetry = true;
                    }
                })
                .setNegativeButton("exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onClickRetry = false;

                    }
                });
        //методдля показа диалога
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public String getNamesCurrencies(Context context){
        String[] currenciesNamesArray = context.getResources().getStringArray(R.array.currenciesArray);
        String currenciesName = "";
        for (int i = 0; i < currenciesNamesArray.length; i++) {
            if (i != (currenciesNamesArray.length - 1))
                currenciesName += currenciesNamesArray[i] + ",";
            else
                currenciesName += currenciesNamesArray[i];
        }
        return currenciesName;
    }

    public String getFilteredNamesCurrencies(String currencyForDelete, Context context) {

        String[] currenciesNamesArray = context.getResources().getStringArray(R.array.currenciesArray);
        String currenciesName = "";

        for (int i = 0; i < currenciesNamesArray.length; i++) {
            if (!currenciesNamesArray[i].equalsIgnoreCase(currencyForDelete)) {
                if (i != (currenciesNamesArray.length - 1)) {
                    currenciesName += currenciesNamesArray[i] + ",";
                } else {
                    currenciesName += currenciesNamesArray[i];
                }
            }
        }
        return currenciesName;
    }
}
