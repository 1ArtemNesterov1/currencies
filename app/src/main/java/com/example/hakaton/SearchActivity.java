package com.example.hakaton;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hakaton.Utils.ApiSerrvice;
import com.example.hakaton.Utils.MaskWatcher;
import com.example.hakaton.Utils.Utils;
import com.example.hakaton.adapter.CurrrencyAdapter;
import com.example.hakaton.interfaces.IGetDataFromFlixer;
import com.example.hakaton.model.ModelCurrency;
import com.example.hakaton.model.RatesModel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity implements IGetDataFromFlixer, CurrrencyAdapter.OnItemClickListener, CurrrencyAdapter.LongClickListener {

    @BindView(R.id.as_codeCurrency)
    TextView txtCodeCurrency;
    @BindView(R.id.as_nameCurrency)
    TextView txtNameCurrency;
    @BindView(R.id.as_listCurrencyes)
    RecyclerView recyclerListCurrency;
    @BindView(R.id.as_imageCurrency)
    ImageView imageCurrency;
    @BindView(R.id.as_search)
    EditText searchCurrency;
    @BindView(R.id.as_searchBtn)
    Button btnSearch;
    Utils utils = new Utils();

    List<RatesModel> ratesModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //метод для привязки лаяута к Активити(вид как будет выглядить)
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        imageCurrency.setImageResource(getIntent().getIntExtra(MainActivity.ICON_CURRENCY, R.drawable.question));
        txtNameCurrency.setText(getIntent().getStringExtra(MainActivity.NAME_CURRENCY));
        txtCodeCurrency.setText(getIntent().getStringExtra(MainActivity.CODE_CURRENCY));

        //формируем новый адаптар
        final CurrrencyAdapter adapter = new CurrrencyAdapter(ratesModelList, this,
                true, SearchActivity.this, SearchActivity.this);

        searchCurrency.addTextChangedListener(new MaskWatcher("####-##-##"));



        //метод для обработки введеных данных
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String[] datesArray = searchCurrency.getText().toString().split("-");

                Calendar calendar = new GregorianCalendar();
                Calendar calendar2 = new GregorianCalendar(Integer.parseInt(datesArray[0]), Integer.parseInt(datesArray[1])-1, Integer.parseInt(datesArray[2]));

                if (calendar2.after(calendar)) {
                    Toast.makeText(SearchActivity.this, "Введите правильно дату", Toast.LENGTH_SHORT).show();
                } else
                    getData(adapter, SearchActivity.this);
            }
        });

    }



    @Override
    public void getData(final CurrrencyAdapter currrencyAdapter, final Context context) {

        final String filteredNamesCurrencies = utils.getFilteredNamesCurrencies(getIntent().getStringExtra(MainActivity.CODE_CURRENCY), context);


        ApiSerrvice.getData("8346ccee731e19db37259397720b49ce", "EUR", searchCurrency.getText().toString(), filteredNamesCurrencies).enqueue(new Callback<ModelCurrency>() {
            @Override
            public void onResponse(Call<ModelCurrency> call, Response<ModelCurrency> response) {

                if (response.isSuccessful() && response.body() != null) {

                    ModelCurrency modelCurrency = response.body();

                    ratesModelList.clear();

                    String[] filteredNames = filteredNamesCurrencies.split(",");

                    for (int i = 0; i < filteredNames.length; i++) {
                        double currencyRate = 0;
                        String addDesString = filteredNames[i].toLowerCase() + "Des";
                        try {
                            Method method = ModelCurrency.Rates.class.getDeclaredMethod("get" + filteredNames[i]);

                            double currencyDouble = Double.parseDouble(String.valueOf(method.invoke(modelCurrency.getRates())));

                            ratesModelList.add(new RatesModel(filteredNames[i], filteredNames[i], addDesString, currencyDouble, context, filteredNames[i]));


                        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }

                    recyclerListCurrency.setAdapter(currrencyAdapter);
                    currrencyAdapter.notifyDataSetChanged();

                } else {
                    utils.ErrorInternet(currrencyAdapter, context);
                    if (utils.isOnClickRetry()) {
                        getData(currrencyAdapter, context);
                    } else finish();
                }
            }

            @Override
            public void onFailure(Call<ModelCurrency> call, Throwable t) {
                utils.ErrorInternet(currrencyAdapter, context);
                if (utils.isOnClickRetry()) {
                    getData(currrencyAdapter, context);
                } else finish();
            }
        });

    }

    @Override
    public void OnItemClick(RatesModel ratesModel) {

    }

    @Override
    public void longItemClick(RatesModel ratesModel) {

    }
}
