package com.example.hakaton.model;

import android.content.Context;
//моделька только одной валюты
public class RatesModel {

    private Context context;
    private String currencyIcon;
    private String currencyCode;
    private String currencyTitle;
    private String currencyDescription;
    private double currencyRate;

    public RatesModel(String currencyIcon, String currencyTitle, String currencyDescription, double currencyRate, Context context, String currencyCode) {
        this.currencyIcon = currencyIcon;
        this.currencyTitle = currencyTitle;
        this.currencyDescription = currencyDescription;
        this.currencyRate = currencyRate;
        this.currencyCode = currencyCode;
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public int getCurrencyIcon() {

        int avatarImageId = context.getResources().getIdentifier(currencyTitle.toLowerCase(), "drawable", context.getPackageName());


        return avatarImageId;
    }

    public void setCurrencyIcon(String currencyIcon) {
        this.currencyIcon = currencyIcon;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyTitle() {
        return currencyTitle;
    }

    public void setCurrencyTitle(String currencyTitle) {
        this.currencyTitle = currencyTitle;
    }

    public String getCurrencyDescription() {
        return currencyDescription;
    }

    public void setCurrencyDescription(String currencyDescription) {
        this.currencyDescription = currencyDescription;
    }

    public double getCurrencyRate() {
        return currencyRate;
    }

    public void setCurrencyRate(double currencyRate) {
        this.currencyRate = currencyRate;
    }


}
