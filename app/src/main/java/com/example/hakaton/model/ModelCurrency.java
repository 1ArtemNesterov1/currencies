package com.example.hakaton.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelCurrency {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("timestamp")
    @Expose
    private Integer timestamp;
    @SerializedName("base")
    @Expose
    private String base;
    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("rates")
    @Expose
    private Rates rates;

    private String info = "";

    public ModelCurrency(Boolean success, Integer timestamp, String base, String date, Rates rates) {
        this.success = success;
        this.timestamp = timestamp;
        this.base = base;
        this.date = date;
        this.rates = rates;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Rates getRates() {
        return rates;
    }

    public void setRates(Rates rates) {
        this.rates = rates;
    }

    public String getInfo() {
        return info;
    }


    public class Rates {

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("timestamp")
        @Expose
        private Integer timestamp;
        @SerializedName("base")
        @Expose
        private String base;
        @SerializedName("date")
        @Expose
        private String date;

        @SerializedName("rates")
        @Expose
        private Rates rates;

        @SerializedName("AED")

        private Double aED;
        @SerializedName("AFN")

        private Double aFN;
        @SerializedName("ALL")

        private Double aLL;
        @SerializedName("AMD")

        private Double aMD;
        @SerializedName("ANG")

        private Double aNG;
        @SerializedName("AOA")
        private Double aOA;

        @SerializedName("ARS")
        @Expose
        private Double ARS;

        @SerializedName("AUD")
        @Expose
        private Double aUD;
        @SerializedName("CAD")
        @Expose
        private Double cAD;
        @SerializedName("EUR")
        @Expose
        private Integer eUR;
        @SerializedName("GBP")
        @Expose
        private Double gBP;
        @SerializedName("SZL")
        @Expose
        private Double sZL;
        @SerializedName("PLN")
        @Expose
        private Double pLN;
        @SerializedName("UAH")
        @Expose
        private Double uAH;
        @SerializedName("USD")
        @Expose
        private Double uSD;

        public Double getpLN() {
            return pLN;
        }

        public void setpLN(Double pLN) {
            this.pLN = pLN;
        }

        public Double getAED() {
            return aED;
        }


        public void setAED(Double aED) {
            this.aED = aED;
        }

        public Double getAFN() {
            return aFN;
        }

        public void setAFN(Double aFN) {
            this.aFN = aFN;
        }

        public Double getALL() {
            return aLL;
        }

        public void setALL(Double aLL) {
            this.aLL = aLL;
        }

        public Double getAMD() {
            return aMD;
        }

        public void setAMD(Double aMD) {
            this.aMD = aMD;
        }

        public Double getuAH() {
            return uAH;
        }

        public void setuAH(Double uAH) {
            this.uAH = uAH;
        }

        public Double getANG() {
            return aNG;
        }

        public void setANG(Double aNG) {
            this.aNG = aNG;
        }

        public Double getAOA() {
            return aOA;
        }

        public void setAOA(Double aOA) {
            this.aOA = aOA;
        }

        public Double getAUD() {
            return aUD;
        }

        public void setAUD(Double aUD) {
            this.aUD = aUD;
        }

        public Double getCAD() {
            return cAD;
        }


        public void setCAD(Double cAD) {
            this.cAD = cAD;
        }

        public Integer getEUR() {
            return eUR;
        }

        public void setEUR(Integer eUR) {
            this.eUR = eUR;
        }

        public Double getGBP() {
            return gBP;
        }

        public void setGBP(Double gBP) {
            this.gBP = gBP;
        }

        public Double getaED() {
            return aED;
        }

        public void setaED(Double aED) {
            this.aED = aED;
        }

        public Double getaFN() {
            return aFN;
        }

        public void setaFN(Double aFN) {
            this.aFN = aFN;
        }

        public Double getaLL() {
            return aLL;
        }

        public void setaLL(Double aLL) {
            this.aLL = aLL;
        }

        public Double getaMD() {
            return aMD;
        }

        public void setaMD(Double aMD) {
            this.aMD = aMD;
        }

        public Double getaNG() {
            return aNG;
        }

        public void setaNG(Double aNG) {
            this.aNG = aNG;
        }

        public Double getaOA() {
            return aOA;
        }

        public void setaOA(Double aOA) {
            this.aOA = aOA;
        }

        public Double getARS() {
            return ARS;
        }

        public void setARS(Double ARS) {
            this.ARS = ARS;
        }

        public Double getaUD() {
            return aUD;
        }

        public void setaUD(Double aUD) {
            this.aUD = aUD;
        }

        public Double getcAD() {
            return cAD;
        }

        public void setcAD(Double cAD) {
            this.cAD = cAD;
        }

        public Integer geteUR() {
            return eUR;
        }

        public void seteUR(Integer eUR) {
            this.eUR = eUR;
        }

        public Double getgBP() {
            return gBP;
        }

        public void setgBP(Double gBP) {
            this.gBP = gBP;
        }

        public Double getsZL() {
            return sZL;
        }

        public void setsZL(Double sZL) {
            this.sZL = sZL;
        }

        public Double getuSD() {
            return uSD;
        }

        public void setuSD(Double uSD) {
            this.uSD = uSD;
        }

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Integer getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Integer timestamp) {
            this.timestamp = timestamp;
        }

        public String getBase() {
            return base;
        }

        public void setBase(String base) {
            this.base = base;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }
    }


}





