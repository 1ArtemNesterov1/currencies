package com.example.hakaton;

import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hakaton.Utils.ApiSerrvice;
import com.example.hakaton.Utils.Utils;
import com.example.hakaton.adapter.CurrrencyAdapter;
import com.example.hakaton.interfaces.IGetDataFromFlixer;
import com.example.hakaton.model.ModelCurrency;
import com.example.hakaton.model.RatesModel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CurrencyInfoActivity extends AppCompatActivity implements CurrrencyAdapter.OnItemClickListener, IGetDataFromFlixer, CurrrencyAdapter.LongClickListener {
    @BindView(R.id.as_imageCurrency)
    ImageView imageCurrency;
    @BindView(R.id.as_listCurrencyes)
    RecyclerView recyclerCurrencys;

    @BindView(R.id.as_codeCurrency)
    TextView txtCodeCurrency;
    @BindView(R.id.aci_description)
    TextView txtDescrCurrency;
    @BindView(R.id.as_nameCurrency)
    TextView txtNameCurrency;

    List<RatesModel> ratesModelList = new ArrayList<>();

    Utils utils = new Utils();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_info);
        ButterKnife.bind(this);


        imageCurrency.setImageResource(getIntent().getIntExtra(MainActivity.ICON_CURRENCY, R.drawable.question));
        txtNameCurrency.setText(getIntent().getStringExtra(MainActivity.NAME_CURRENCY));
        txtDescrCurrency.setText(getIntent().getStringExtra(MainActivity.DESCRIPTION_CURRENCY));
        txtCodeCurrency.setText(getIntent().getStringExtra(MainActivity.CODE_CURRENCY));

        CurrrencyAdapter newAdapter = new CurrrencyAdapter(ratesModelList, CurrencyInfoActivity.this, true, this, this);

        getData(newAdapter, getApplicationContext());


    }

    @Override
    public void OnItemClick(RatesModel ratesModel) {

    }

    @Override
    public void getData(final CurrrencyAdapter currrencyAdapter, final Context context) {

        final String filteredNamesCurrencies = utils.getFilteredNamesCurrencies(getIntent().getStringExtra(MainActivity.CODE_CURRENCY), context);

//        String nameForDelivery = txtCodeCurrency.getText().toString(); //Используем для запроса в платной версии API Fixer.io

        ApiSerrvice.getData("8346ccee731e19db37259397720b49ce", "EUR", "", filteredNamesCurrencies).enqueue(new Callback<ModelCurrency>() {
            @Override
            public void onResponse(Call<ModelCurrency> call, Response<ModelCurrency> response) {

                if (response.isSuccessful() && response.body() != null) {

                    ModelCurrency modelCurrency = response.body();

                    String[] filteredNames = filteredNamesCurrencies.split(",");

                    for (int i = 0; i < filteredNames.length; i++) {
                        double currencyRate = 0;
                        String addDesString = filteredNames[i].toLowerCase() + "Des";
                        try {
                            Method method = ModelCurrency.Rates.class.getDeclaredMethod("get" + filteredNames[i]);

                            double currencyDouble = Double.parseDouble(String.valueOf(method.invoke(modelCurrency.getRates())));

                            ratesModelList.add(new RatesModel(filteredNames[i], filteredNames[i], addDesString, currencyDouble, context, filteredNames[i]));


                        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }

                    recyclerCurrencys.setAdapter(currrencyAdapter);

                } else {
                    utils.ErrorInternet(currrencyAdapter, context);
                    if (utils.isOnClickRetry()) {
                        getData(currrencyAdapter, context);
                    } else finish();
                }
            }

            @Override
            public void onFailure(Call<ModelCurrency> call, Throwable t) {
                utils.ErrorInternet(currrencyAdapter, context);
                if (utils.isOnClickRetry()) {
                    getData(currrencyAdapter, context);
                } else finish();
            }
        });


    }

    @Override
    public void longItemClick(RatesModel ratesModel) {

    }
}
